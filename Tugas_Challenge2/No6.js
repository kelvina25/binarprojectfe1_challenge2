// Buatlah sebuah function yang berfungsi untuk mendapatkan angka terbesar kedua dari sebuah array. 

// Misal diberikan sebuah array yang terdiri dari beberapa angka [2,3,5,6,6,4], berdasarkan data dari array tersebut dapat kita simpulkan bahwasanya angka terbesar dari array tersebut adalah 6, angka kedua terbesar adalah 5, dan angka ketiga terbesar adalah 4. Maka dari itu function yang akan kamu buat ini akan me-return angka kedua terbesar pada array yang telah diberikan, yaitu angka 5.

// Function ini akan menerima satu parameter, yaitu:
// 1. dataNumbers ⇒ Array yang berisi beberapa angka

// Kriteria function:
// 1. Beri nama function tersebut getAngkaTerbesarKedua
// 2. function ini harus return data dengan tipe number
// 3. function ini harus return data angka terbesar kedua dari angka-angka yang diberikan di dalam array
// 4. function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.


function getAngkaTerbesarKedua(dataNumbers) {
  if (dataNumbers === undefined)
    return "Error: function need an array parameter";

  if (!Array.isArray(dataNumbers)) 
    return "Error: Invalid data type";

  if (dataNumbers.length < 2)
    return "Error: Array length should be 2 or longer";

  let first = Number.MIN_SAFE_INTEGER;
  let second = Number.MIN_SAFE_INTEGER;
  let isContaintMinVal = false;
  dataNumbers.forEach((number) => {
    if (number > first) {
      first = number;

    } else if (number !== first && number > second) {
      second = number;

    } else if (number === Number.MIN_SAFE_INTEGER) {
      isContaintMinVal = true;
    }
  });

  if (second !== Number.MIN_SAFE_INTEGER || isContaintMinVal) return second;
  
  else return "there is no second largest number";
}

const dataNumbers = [9, 4, 7, 7, 4, 3, 2, 2, 8];
console.log(getAngkaTerbesarKedua(dataNumbers));
console.log(getAngkaTerbesarKedua(0));
console.log(getAngkaTerbesarKedua());
