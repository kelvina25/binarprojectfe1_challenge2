// Buatlah sebuah function yang berfungsi untuk membagikan sebuah nama menjadi Nama Depan, Nama Tengah, Nama Belakang. Function ini nantinya akan menerima satu parameter yang berisi nama lengkap seseorang. Apabila nama lengkap dari seseorang tersebut lebih dari 3 suku kata, maka function tersebut harus menghasilkan sebuah error. Tapi apabila parameter yang diberikan valid (tidak lebih dari 3 suku kata), maka function ini akan menghasilkan sebuah object dengan properti firstName, middleName, lastName. 

// function ini akan menerima satu parameter, yaitu:
// 1. givenNumber ⇒ angka yang akan di-check oleh function

// Kriteria function:
// Beri nama function tersebut getSplitName
// 1. function ini harus return data dengan tipe object
// 2. function ini harus return data dengan tipe object
// 3. function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.




function getSplitName(personName) {
  if (typeof personName !== "string") 
    return "Error: invalid data type";

  const splittedName = personName.split(" ");
  
  if (splittedName.length > 3) {
    return "Error : this function is only for 3 characters name";
  
  } else if (splittedName.length === 3) {
    return {
      firstName: splittedName[0],
      middleName: splittedName[1],
      lastName: splittedName[2],
    };

  } else if (splittedName.length === 2) {
    return {
      firstName: splittedName[0],
      middleName: null,
      lastName: splittedName[1],
    };

  } else if (splittedName.length === 1) {
    return { firstName: splittedName[0], middleName: null, lastName: null };
  }
}
console.log(getSplitName("Adi Daniela Pranata kusuma"));
console.log(getSplitName("Adi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName(0));
console.log(getSplitName(""));
