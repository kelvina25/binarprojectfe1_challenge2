// Hari ini Toko buku milik Ibu Daniela berhasil menjual banyak sekali buku-buku novel. Gambar disamping adalah  data penjualan buku-buku novel yang dijual di Toko buku milik Ibu Daniela, dalam format array of object.

// Tugas kamu adalah membuat sebuah function yang berfungsi membantu Ibu Daniela untuk mendapatkan informasi berupa Total Keuntungan, Total Modal, Produk Buku Terlaris, Penulis Buku Terlaris dan Persentase Keuntungan dari data penjualan yang telah disediakan diatas.

// Function yang kamu buat ini akan me-return sebuah data yang berbentuk sebuah object yang dari beberapa properti.

// Function ini akan menerima satu parameter, yaitu:
// 1. dataPenjualan ⇒ Array yang berisi beberapa object, object terdiri dari properti yang memiliki informasi penjualan product.

// Kriteria function:
// 1. Beri nama function tersebut getInfoPenjualan
// 2. function ini harus return data dengan tipe object
// 3. Data object harus berisi properti seperti yang dijelaskan sebelumnya yaitu totalKeuntungan, totalModal, produkBukuTerlaris, persentaseKeuntungan, dan penulisTerlaris
// 4. function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.


const dataPenjualanNovel = [
    {
      idProduk: "BOOK002421",
      namaProduk: "Pulang - Pergi",
      penulis: "Tere Liye",
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduk: "BOOK002351",
      namaProduk: "Selamat Tinggal",
      penulis: "Tere Liye",
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduk: "BOOK002941",
      namaProduk: "Garis Waktu",
      penulis: "Fiersa Besari",
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduk: "BOOK002942",
      namaProduk: "Laskar Pelangi",
      penulis: "Andrea Hirata",
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];
  console.log(getInfoPenjualan(dataPenjualanNovel));
  
  function getInfoPenjualan(dataPenjualan) {

    if (!Array.isArray(dataPenjualan)) {
      console.error("Invalid data type");
      return {};
    }

    let totalKeuntungan = 0;
    let totalModal = 0;
    let produkTerlaris = dataPenjualan[0];
    let persentaseKeuntungan = 0;
    let penulisBuku = {};
    let penulisTerlaris = "";
  
    dataPenjualan.forEach((data) => {
      modal = data.hargaBeli * (data.totalTerjual + data.sisaStok);
      keuntungan = data.hargaJual * data.totalTerjual - modal;
      totalModal += modal;
      totalKeuntungan += keuntungan;

      if (data.totalTerjual > produkTerlaris.totalTerjual) {
        produkTerlaris = data;
      }
      
      if (penulisBuku[data.penulis])
        penulisBuku[data.penulis] += data.totalTerjual;

      else penulisBuku[data.penulis] = data.totalTerjual;
    });
    
    console.log(penulisBuku);
    persentaseKeuntungan = (totalKeuntungan / totalModal) * 100;
    penulisTerlaris = Object.keys(penulisBuku).reduce((a, b) => {
      return penulisBuku[a] > penulisBuku[b] ? a : b;
    });
  
    const result = {
      totalKeuntungan: "Rp. " + totalKeuntungan.toLocaleString("id-ID"),
      totalModal: "Rp. " + totalModal.toLocaleString("id-ID"),
      persentaseKeuntungan: Math.round(persentaseKeuntungan) + "%",
      produkTerlaris: produkTerlaris.namaProduk,
      penulisTerlaris: penulisTerlaris,
    };
    return result;
  }
  