// Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah password yang diberikan sebagai parameter memenuhi kriteria yang telah ditentukan atau tidak. 

// Berikut kriteria password yang valid, apabila password tidak memenuhi kriteria dibawah ini, maka dinyatakan tidak valid:
// - Password harus memiliki panjang minimal 8 huruf
// - Password harus memiliki minimal 1 huruf besar
// - Password harus memiliki minimal 1 huruf kecil
// - Password harus memiliki minimal 1 angka

// function ini akan menerima satu parameter, yaitu:
// givenPassword ⇒ Password berupa string dan akan dicek oleh function tersebut.

// Kriteria function:
// 1. Beri nama function tersebut isValidPassword
// 2. function ini harus return data dengan tipe Boolean
// 3. function ini harus return data berupa true ATAU false
// 4. function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.


function isValidPassword(password) {
    if (password === undefined) {
      console.log("Error: there is no password");
      return false;
    }
    if (typeof password !== "string") {
      console.log("Error: A password should be in string type");
      return false;
    }
    const isValid = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\w\W]{8,}$/.test(password);
    return isValid;
  }
  console.log(isValidPassword("Meong@2021"));
  console.log(isValidPassword("@eong"));
  console.log(isValidPassword("Meong2"));
  console.log(isValidPassword(0));
  console.log(isValidPassword());
  