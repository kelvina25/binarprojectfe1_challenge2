// Buatlah sebuah function yang berfungsi untuk melakukan pengecekan apakah alamat email yang diberikan sebagai parameter, adalah alamat email yang formatnya benar atau tidak. 

// Jika parameter yang diberikan adalah alamat email yang benar, maka function ini harus return VALID, namun apabila alamat email yang diberikan tidak benar formatnya, maka function harus return INVALID

// function ini akan menerima satu parameter, yaitu:
// 1. email ⇒ email yang akan dicek oleh function

// Kriteria function yang harus kamu buat: 
// function ini harus return data dengan tipe string
// function ini harus return data dengan tipe string
// function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.


function checkEmail(email) {
    
    if (email === undefined) {
      return "Error: There is no email to check";
    }
    if (typeof email !== "string") {
      return "Error: Email should be a string";
    }
    if (!/[@]/.test(email)) {
      return "Error: Email should containt '@' character";
    }
    
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
      return "VALID";

    } else {
      
      return "INVALID";
    }
  }
  
  console.log(checkEmail("apranata@binar.co.id"));
  console.log(checkEmail("apranata@binar.com"));
  console.log(checkEmail('apranata@binar'));
  console.log(checkEmail('apranata'));
  console.log(checkEmail());
  