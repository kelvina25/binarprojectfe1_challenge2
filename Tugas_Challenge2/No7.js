// Hari ini Toko Pak Aldi berhasil menjual banyak sepatu. Pada gambar disamping terdapat data sepatu-sepatu yang terjual dari toko Pak Aldi dalam bentuk array of object. 

// Tugas kamu adalah membuat sebuah function yang berfungsi membantu Pak Aldi untuk menghitung total seluruh sepatu yang terjual.

// Function ini akan menerima satu parameter, yaitu:
// 1. dataPenjualan ⇒ Array yang berisi beberapa object, object terdiri dari properti yang memiliki informasi penjualan product.

// Kriteria function:
// 1. Beri nama function tersebut getTotalPenjualan
// 2. function ini harus return data dengan tipe number
// 3. function ini harus return total penjumlahan dari properti totalTerjual pada data yang diberikan
// 4. function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.


const dataPenjualanPakAldi = [
    {
      namaProduct: "Sepatu Futsal Nike Vapor Academy 8",
      hargaSatuan: 760000,
      kategori: "Sepatu Sport",
      totalTerjual: 90,
    },
    {
      namaProduct: "Sepatu Warrior Tristan Black Brown High",
      hargaSatuan: 960000,
      kategori: "Sepatu Sneaker",
      totalTerjual: 37,
    },
    {
      namaProduct: "Sepatu Warrior Tristan Maroon High",
      hargaSatuan: 360000,
      kategori: "Sepatu Sneaker",
      totalTerjual: 90,
    },
    {
      namaProduct: "Sepatu Warrior Rainbow Tosca Corduroy",
      hargaSatuan: 960000,
      kategori: "Sepatu Sneaker",
      totalTerjual: 90,
    },
  ];
  
  function getTotalPenjualan(dataPenjualan) {

    if (!Array.isArray(dataPenjualan))
      return "Error : data penjualan harus berupa array";
    
    const totalPenjualan = dataPenjualan.reduce((total, data) => {
      return total + data.totalTerjual;
    }, 0);

    if (isNaN(totalPenjualan)) return "Error: format data salah !";

    else return totalPenjualan;
  }
  
  console.log(getTotalPenjualan(dataPenjualanPakAldi));
  