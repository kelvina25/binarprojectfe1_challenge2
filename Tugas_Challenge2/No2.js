// Buatlah sebuah function yang berfungsi mendeteksi apakah sebuah angka termasuk angka genap atau ganjil. 

// Function ini akan menerima satu parameter, yaitu :

// 1. givenNumber ⇒ angka yang akan dicheck oleh function

// Kriteria function yang harus kamu buat: 
// Beri nama function tersebut checkTypeNumber
// Menggunakan arrow function
// function ini harus return data dengan tipe string
// function ini harus retrun data berupa GENAP ATAU GANJIL
// function ini harus memiliki validasi terhadap tipe data dari parameter yang diterima.

const checkTypeNumber = (givenNumber) => {
    if (givenNumber === undefined) {
      return "Error : Bro where is the parameter?";

    } else if (typeof givenNumber !== "number") {
      return "Error: Invalid data type";
      
    } else return givenNumber % 2 === 0 ? "Genap" : "Ganjil";
  };
  
  console.log(checkTypeNumber(10));
  console.log(checkTypeNumber(3));
  console.log(checkTypeNumber("3"));
  console.log(checkTypeNumber({}));
  console.log(checkTypeNumber([]));
  console.log(checkTypeNumber());
  