// Buatlah sebuah function dengan nama changeWord yang berfungsi untuk menggantikan sebuah kata didalam sebuah kalimat.

// Function ini akan menerima 3 parameter, yaitu :

// selectedText => Kata yang terdapat pada sebuah kalimat dan merupakan kata yang akan diganti nantinya.
// changedText => Kata yang akan menjadi pengganti pada sebuah kalimat nantinya.
// text => Sebuah kalimat.

function changeWord(selectedText, changedText, text) {
    return text.replace(selectedText, changedText);
  }
  
  const kalimat1 = "Andini sangat mencintai kamu selamanya";
  const kalimat2 = "Gunung bromo tak kan mampu menggambarkan besarnya cintaku padamu";
  
  console.log(changeWord("mencintai", "membenci", kalimat1));
  console.log(changeWord("bromo", "semeru", kalimat2));
  